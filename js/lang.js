function getCookie(name) {
    var cookie = " " + document.cookie;
    var search = " " + name + "=";
    var setStr = null;
    var offset = 0;
    var end = 0;
    if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = cookie.indexOf(";", offset)
            if (end == -1) {
                end = cookie.length;
            }
            setStr = unescape(cookie.substring(offset, end));
        }
    }
    return(setStr);
}
function setCookie(name, value, expires, path, domain, secure) {
    var cookie = name + "=" + encodeURIComponent(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
    document.cookie = cookie;
}
$(document).ready(function($){
  var cookie = getCookie("lang");
  if(cookie == null){
    setCookie("lang", "en", "10/01/2077", "/");
  }


});
(function(){
  var body = document.getElementsByTagName('body')[0];

  var switcherRU = document.getElementById('switcher-ru');
  var switcherEN = document.getElementById('switcher-en');

  var y = getCookie("lang");
  if(y===null){
    body.className = "ru";
  }
  else {
    lang = getCookie("lang");
    body.className = lang;
  }
  var titleRU = document.getElementById('title-ru');
  var titleEN = document.getElementById('title-en');
  var title = {
    ru: titleRU.getAttribute('content'),
    en: titleEN.getAttribute('content')
  }

  var switchLang = function() {

    var lang = this.id
    lang = lang.replace('switcher-', '');

    // Меняем класс для BODY
    body.className = lang;

    setCookie("lang", lang, "10/01/2077", "/");
    document.title = title[lang];
  }

  switcherRU.onclick = switchLang;
  switcherEN.onclick = switchLang;
})();
